﻿using Base.View;
using Gameplay.Gesture.Configs;
using Leopotam.Ecs;
using Zenject;

namespace Base.Zenject
{
    public class StartupInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInstance(new EcsWorld());

            Container.Bind<CharacterConfig>().FromResources("CharacterConfig");
            Container.Bind<GestureListConfig>().FromResources("GestureListConfig");

            Container.Bind<RootView>()
                .FromComponentInHierarchy()
                .AsSingle();
        }
    }
}
