﻿using Gameplay.Gesture.Systems;
using Zenject;

namespace Gameplay.Zenject
{
    public class GameplayInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GestureRecognizeSystem>().AsSingle();
            Container.Bind<GestureEntityCreatorSystem>().AsSingle();
            
            Container.BindInterfacesAndSelfTo<GameplaySystemsExecutor>()
                .AsSingle()
                .NonLazy();
        }
    }
}