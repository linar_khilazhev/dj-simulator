﻿using System;
using Gameplay.Gesture.Systems;
using Leopotam.Ecs;
using Zenject;

namespace Gameplay.Zenject
{
    public class GameplaySystemsExecutor : ITickable, IDisposable
    {
        private readonly EcsSystems _systems;

        public GameplaySystemsExecutor(EcsWorld world, 
            GestureRecognizeSystem gestureRecognizeSystem,
            GestureEntityCreatorSystem gestureEntityCreatorSystem)
        {
            _systems = new EcsSystems(world, "Gameplay Systems");

            _systems
                .Add(gestureRecognizeSystem)
                .Add(gestureEntityCreatorSystem);

            _systems.Init();
        }

        public void Dispose()
        {
            _systems.Destroy();
        }

        public void Tick()
        {
            _systems.Run();
        }
    }
}