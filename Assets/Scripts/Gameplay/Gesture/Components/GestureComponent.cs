﻿using UnityEngine;

namespace Gameplay.Gesture.Components
{
    public struct GestureComponent
    {
        public GameObject Character;
        public string GestureName;
    }
}