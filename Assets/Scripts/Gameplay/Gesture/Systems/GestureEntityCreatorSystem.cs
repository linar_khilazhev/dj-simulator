﻿using System.Collections.Generic;
using Gameplay.Gesture.Components;
using Gameplay.Gesture.Configs;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Gesture.Systems
{
    public class GestureEntityCreatorSystem : IEcsInitSystem
    {
        private EcsWorld _world;
        private EcsFilter<GestureComponent> _gestureComponentFilter;

        private GestureListConfig _gestureListConfig;
        private CharacterConfig _characterConfig;

        public GestureEntityCreatorSystem(GestureListConfig gestureListConfig, CharacterConfig characterConfig)
        {
            _gestureListConfig = gestureListConfig;
            _characterConfig = characterConfig;
        }

        private List<string> _gestureNames;
        private int _gestureCount = 5;
            
        public void Init()
        {
            Debug.Log("Okay");
            CreateGesturesComponents();
        }

        private void CreateGesturesComponents()
        {
            for (int i = 0; i < _gestureCount; i++)
            {
                CreateGestureEntity();
            }
        }

        private void CreateGestureEntity()
        {
            var gestureEntity = _world.NewEntity();
            ref var gestureComponent = ref gestureEntity.Get<GestureComponent>();

            var randomGestureConfig =
                _gestureListConfig.GestureConfigs[Random.Range(0, _gestureListConfig.GestureConfigs.Count)];

            gestureComponent.GestureName = randomGestureConfig.GestureName;
            gestureComponent.Character =
                Object.Instantiate(_characterConfig.Prefab, new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)), Quaternion.identity);

            var gestureGameObject =
                Object.Instantiate(randomGestureConfig.Prefab, Vector3.up * 5.0f, Quaternion.identity);
            gestureGameObject.transform.SetParent(gestureComponent.Character.transform, false);
            
            
            Debug.Log($"We need to draw {gestureComponent.GestureName}");
        }
    }
}