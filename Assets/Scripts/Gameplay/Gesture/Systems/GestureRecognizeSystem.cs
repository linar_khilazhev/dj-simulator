﻿using Doozy.Engine;
using Gameplay.Gesture.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Gesture.Systems
{
    public class GestureRecognizeSystem : IEcsInitSystem
    {
        private EcsFilter<GestureComponent> _gestureComponentFilter;
        public void Init()
        {
            Message.AddListener<GameEventMessage>(HandleMessage);
        }

        private void HandleMessage(GameEventMessage eventMessage)
        {
            // Debug.Log($"We catched new event {eventMessage.EventName}");
            CheckComponents(eventMessage.EventName);
        }

        private void CheckComponents(string gestureName)
        {
            foreach (var index in _gestureComponentFilter)
            {
                ref var gestureComponent = ref _gestureComponentFilter.Get1(index);
                ref var entity = ref _gestureComponentFilter.GetEntity(index);
                
                if (gestureComponent.GestureName == gestureName)
                {
                    Debug.Log("We recognized gesture");
                    gestureComponent.Character.GetComponent<Renderer>().material.color = Color.green;
                    // Object.Destroy(gestureComponent.Character);
                    entity.Destroy();
                }
                else
                {
                    Debug.Log($"We need to draw {gestureComponent.GestureName}");
                }
            }

            if (_gestureComponentFilter.IsEmpty())
            {
                Debug.Log("We found all gestures");
            }
        }
    }
}