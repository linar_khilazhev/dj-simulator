﻿using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Gesture.Configs
{
    [CreateAssetMenu(fileName = "GestureListConfig", menuName = "Gameplay/Gesture/Configs/GestureListConfig", order = 0)]
    public class GestureListConfig : ScriptableObject
    {
        [SerializeField] private List<GestureConfig> gestureConfigs;

        public List<GestureConfig> GestureConfigs => gestureConfigs;
    }
}