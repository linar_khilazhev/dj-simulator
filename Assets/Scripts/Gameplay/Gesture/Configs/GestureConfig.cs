﻿using UnityEngine;

namespace Gameplay.Gesture.Configs
{
    [CreateAssetMenu(fileName = "GestureConfig", menuName = "Gameplay/Gesture/Configs/GestureConfig", order = 0)]
    public class GestureConfig : ScriptableObject
    {
        [SerializeField] private GameObject prefab;
        [SerializeField] private string gestureName;

        public GameObject Prefab => prefab;
        public string GestureName => gestureName;
    }
}