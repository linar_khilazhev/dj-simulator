﻿using UnityEngine;

namespace Gameplay.Gesture.Configs
{
    [CreateAssetMenu(fileName = "CharacterConfig", menuName = "Gameplay/Gesture/Configs/CharacterConfig", order = 0)]
    public class CharacterConfig : ScriptableObject
    {
        [SerializeField] private GameObject prefab;

        public GameObject Prefab => prefab;
    }
}